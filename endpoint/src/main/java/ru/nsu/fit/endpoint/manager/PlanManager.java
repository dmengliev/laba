package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import java.util.List;
import java.util.UUID;

public class PlanManager extends ParentManager {
    public PlanManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
    /* details - длина не больше 1024 символов и не меньше 1 включительно;
    /* fee - больше либо равно 0 но меньше либо равно 999999.
     */
    public PlanPojo createPlan(PlanPojo plan) {
        Validate.notNull(plan, "Argument 'plan' is null.");

        Validate.notNull(plan.name, "Name is null");
        Validate.isTrue(plan.name.length() >= 2 && plan.name.length() <= 128, "Name's length should be more or equal 2 symbols and less or equal 128 symbols.");

        Validate.notNull(plan.details, "details is null");
        Validate.isTrue(plan.details.length() >= 1 && plan.details.length() <= 1024, "Details's length should be more or equal 1 symbols and less or equal 1024 symbols.");

        return dbService.createPlan(plan);
    }

    public void updatePlan(PlanPojo plan) {
        Validate.notNull(plan, "Argument 'plan' is null.");
    }

    public void removePlan(UUID id) {
        Validate.notNull(id, "Argument 'id' is null.");
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */
    public List<PlanPojo> getPlans(UUID customerId) {
        throw new NotImplementedException("Please implement the method.");
    }
}
