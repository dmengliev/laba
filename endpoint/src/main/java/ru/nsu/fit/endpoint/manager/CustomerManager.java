package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;

import java.util.List;
import java.util.UUID;

public class CustomerManager extends ParentManager {
    public CustomerManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */
    public CustomerPojo createCustomer(CustomerPojo customer) {
        Validate.notNull(customer, "Argument 'customerData' is null.");

        Validate.notNull(customer.firstName, "FirstName is null");
        Validate.isTrue(customer.firstName.length() >= 2 && customer.firstName.length() < 13, "FirstName's length should be more or equal 2 symbols and less or equal 12 symbols.");
        Validate.isTrue(!(customer.firstName).matches("[0-9]+"), "FirstName contains only digits");

        Validate.notNull(customer.pass, "Password is null");
        Validate.isTrue(customer.pass.length() >= 6 && customer.pass.length() < 13, "Password's length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(!customer.pass.equalsIgnoreCase("123qwe"), "Password is easy.");

        Validate.notNull(customer.lastName, "LastName is null");
        Validate.isTrue(customer.lastName.length() >= 2 && customer.lastName.length() < 13, "LastName's length should be more or equal 2 symbols and less or equal 12 symbols.");
        Validate.isTrue(!customer.lastName.matches("[0-9]+"), "LastName contains only digits");
        Validate.notNull(customer.login, "Login is null");


        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<CustomerPojo> getCustomers() {
        //Validate.notNull( "object is not null");
        return dbService.getCustomers();
    }

    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public CustomerPojo updateCustomer(CustomerPojo customer) {
        Validate.notNull(customer, "object is null");
        // CustomerPojo createCustomerInput = new CustomerPojo();
        return dbService.updateCustomer(customer);
    }

    public UUID removeCustomer(UUID id) {
        // РЕАЛИЗОВАТЬ ФУНКЦИЮs
        Validate.notNull(id, "id is null");
        return dbService.removeCustomer(id);
    }

    public UUID getCustomerIdByLogin(String customerLogin) {
        Validate.notNull(customerLogin, "object is null");
       return  dbService.getCustomerIdByLogin(customerLogin);
    }

    public void topUpBalance(UUID customerId, int amount) {
        Validate.notNull(customerId, "Id is null");
        Validate.isTrue((amount <= 0), "Amount must be bigger than 1");
        //Validate.isTrue(!amount.matches("[0-9]+"), "FirstName contains only digits");

    }

}
