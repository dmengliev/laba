package ru.nsu.fit.endpoint.manager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class CustomerManagerTest {
    protected IDBService dbService;
    protected CustomerManager customerManager;

    protected CustomerPojo createCustomerInput;

    @BeforeEach
    void init() {
        // create stubs for the test's class
        dbService = mock(IDBService.class);
        Logger logger = mock(Logger.class);
        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;
    }

    @Test
    void testCreateCustomer() {
        // настраиваем mock.

        CustomerPojo createCustomerOutput = new CustomerPojo();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.firstName = "John";
        createCustomerOutput.lastName = "Wick";
        createCustomerOutput.login = "john_wick@gmail.com";
        createCustomerOutput.pass = "Baba_Jaga96";
        createCustomerOutput.balance = 0;

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

        // Проверяем результат выполенния метода
        assertEquals(customer.id, createCustomerOutput.id);
  //      assertEquals(customer2.id,dbService);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).createCustomer(createCustomerInput);

        // Проверяем, что другие методы не вызывались...
        verify(dbService, times(0)).getCustomers();
    }

    // Как не надо писать тест...
    // Используйте expected exception аннотации или expected exception rule...
    @Test
    void testCreateCustomerWithNullArgument_Wrong() {
        try {
            customerManager.createCustomer(null);
        } catch (IllegalArgumentException ex) {
            assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    @Test
    void testCreateCustomerWithNullArgument_Right() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(null));
        assertEquals("Argument 'customerData' is null.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithShortPassword() {
        createCustomerInput.pass = "123qwe";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password is easy.", exception.getMessage());
    }


    //my tests//
    //NULL FirstName
    @Test
    void testCreateCustomerWithNullFirstName() {
        createCustomerInput.firstName = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("FirstName is null", exception.getMessage());
    }

    //Short FirstName
    @Test
    void testCreateCustomerWithShortFirstName() {
        createCustomerInput.firstName = "a";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("FirstName's length should be more or equal 2 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    //Long FirstName
    @Test
    void testCreateCustomerWithLongFirstName() {
        createCustomerInput.firstName = "DavidBeckhamisgood";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("FirstName's length should be more or equal 2 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    //Digits FirstName
    @Test
    void testCreateCustomerWithDigitsFirstName() {
        createCustomerInput.firstName = "1233";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("FirstName contains only digits", exception.getMessage());
    }

    //NULL PASSWORD
    @Test
    void testCreateCustomerWithNullPassword() {
        createCustomerInput.pass = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password is null", exception.getMessage());
    }

    //NULL LastNAME
    @Test
    void testCreateCustomerWithNullLastName() {
        createCustomerInput.lastName = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("LastName is null", exception.getMessage());

    }
    //Short LastNAME
    @Test
    void testCreateCustomerWithShortLastName() {
        createCustomerInput.lastName = "a";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("LastName's length should be more or equal 2 symbols and less or equal 12 symbols.", exception.getMessage());
    }
    //Long LastNAME
    @Test
    void testCreateCustomerWithLongLastName() {
        createCustomerInput.lastName = "DavidBeckhamisgood";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("LastName's length should be more or equal 2 symbols and less or equal 12 symbols.", exception.getMessage());
    }
    //Digits LastNAME
    @Test
    void testCreateCustomerWithDigitsLastName() {
        createCustomerInput.lastName = "1234";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("LastName contains only digits", exception.getMessage());
    }

    //null login
    @Test
    void testCreateCustomerWithNullLogin() {
        createCustomerInput.login = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Login is null", exception.getMessage());
    }


    @Test
    void testGetCustomerIdByLogin() {
        // настраиваем mock.
        //UUID login = UUID.fromString("john_wick@gmail.com");
        String login = "john_wick@gmail.com";
        UUID ids = UUID.randomUUID();
        when(dbService.getCustomerIdByLogin(login)).thenReturn(ids);

        // Вызываем метод, который хотим протестировать
        UUID customerName = customerManager.getCustomerIdByLogin(login);

        // Проверяем результат выполенния метода
        assertEquals(customerName, ids);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).getCustomerIdByLogin(createCustomerInput.login);

        // Проверяем, что другие методы не вызывались...
        verify(dbService, times(0)).getCustomers();
    }

    //GetId
    @Test
    void testGetCustomerIdByLoginWithNull() {
        // createCustomerInput = new CustomerPojo();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.getCustomerIdByLogin(null));
        assertEquals("object is null", exception.getMessage());
    }

    //Remove
    @Test
    void testRemoveCustomer() {
        // настраиваем mock.
        CustomerPojo createCustomerOutput = new CustomerPojo();

        UUID customerId = UUID.randomUUID();
        UUID customerOutId  = UUID.randomUUID();

        when(dbService.removeCustomer(customerId)).thenReturn(customerOutId);

        // Вызываем метод, который хотим протестировать
        UUID customerIdNew = customerManager.removeCustomer(customerId);

        // Проверяем результат выполенния метода
        assertEquals(customerIdNew, customerOutId);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).removeCustomer(customerId);

        // Проверяем, что другие методы не вызывались...
        verify(dbService, times(0)).getCustomers();
    }


    //RemoveCustomer
    @Test
    void testRemoveCustomerWithNullId() {
        createCustomerInput.id = null; //new CustomerPojo();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.removeCustomer(createCustomerInput.id));
        assertEquals("id is null", exception.getMessage());
    }

    //topUpBalamce
    @Test
    void testTopUpBalanceWithNullId() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.topUpBalance(null,1));
        assertEquals("Id is null", exception.getMessage());
    }

    //topUpBalamce
    @Test
    void testTopUpBalanceWithNullAmount() {
        //createCustomerInput = new CustomerPojo();
        // UUID customerId = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.topUpBalance(createCustomerInput.id,0));
        assertEquals("Id is null", exception.getMessage());
    }

}
