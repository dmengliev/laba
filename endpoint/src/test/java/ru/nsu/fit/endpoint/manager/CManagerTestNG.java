package ru.nsu.fit.endpoint.manager;


import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CManagerTestNG extends CustomerManagerTest {
    //private CustomerManager customerManager;

    @Test(description = "Try to log into the system",dependsOnMethods= "testCreateCustomer ")
    public void testGetCustomers() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.getCustomers());
        assertEquals("Wrong operation", exception.getMessage());
    }
}
