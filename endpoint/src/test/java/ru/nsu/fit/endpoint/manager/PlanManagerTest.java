package ru.nsu.fit.endpoint.manager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class PlanManagerTest {
    private IDBService dbService;
    private PlanManager planManager;

    private PlanPojo planPojoInput;

    @BeforeEach
    void init() {
        // create stubs for the test's class
        dbService = mock(IDBService.class);
        Logger logger = mock(Logger.class);
        // create the test's class
        planManager = new PlanManager(dbService, logger);

        planPojoInput = new PlanPojo();
        planPojoInput.name = "Plan1";
        planPojoInput.details = "This is the first detail";
        planPojoInput.fee = 0;
    }

    @Test
    void testCreatePlan() {
        // настраиваем mock.
        PlanPojo planPojoOutput = new PlanPojo();
        planPojoOutput.id = UUID.randomUUID();
        planPojoOutput.name = "Plan1";
        planPojoOutput.details = "This is the first detail";
        planPojoOutput.fee = 0;

        when(dbService.createPlan(planPojoInput)).thenReturn(planPojoOutput);

        // Вызываем метод, который хотим протестировать
        PlanPojo planPojo = planManager.createPlan(planPojoInput);

        // Проверяем результат выполенния метода
        assertEquals(planPojo.id, planPojoOutput.id);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).createPlan(planPojoInput);

        // Проверяем, что другие методы не вызывались...
        verify(dbService, times(0)).updateCustomer(null);

    }

    @Test
    void testCreatePlanWithNullName() {
        planPojoInput.name = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                planManager.createPlan(planPojoInput));
        assertEquals("Name is null", exception.getMessage());
    }

    @Test
    void testCreatePlanWithShortName() {
        planPojoInput.name = "a";
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                planManager.createPlan(planPojoInput));
        assertEquals("Name's length should be more or equal 2 symbols and less or equal 128 symbols.", exception.getMessage());
    }


    @Test
    void testCreatePlanWithNullDetails() {
        planPojoInput.details = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                planManager.createPlan(planPojoInput));
        assertEquals("details is null", exception.getMessage());
    }
    ////
    @Test
    void testCreatePlanWithNullArgument() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                planManager.createPlan(null));
        assertEquals("Argument 'plan' is null.", exception.getMessage());
    }

    @Test
    void testUpdatePlanWithNullArgument() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                planManager.updatePlan(null));
        assertEquals("Argument 'plan' is null.", exception.getMessage());
    }

    @Test
    void testRemovePlanWithNullArgument() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                planManager.removePlan(null));
        assertEquals("Argument 'id' is null.", exception.getMessage());
    }

}
